# Translation of docs_kdenlive_org_glossary___useful_information___the_library.po to Catalan
# Copyright (C) 2021 This_file_is_part_of_KDE
# Licensed under the <a href="https://creativecommons.org/licenses/by-sa/4.0/">Creative Commons License SA 4.0</a> unless stated otherwise
# This file is distributed under the same license as the Kdenlive Manual package.
#
# Josep M. Ferrer <txemaq@gmail.com>, 2021.
# Antoni Bella Pérez <antonibella5@yahoo.com>, 2021.
msgid ""
msgstr ""
"Project-Id-Version: documentation-docs-kdenlive-org\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2021-12-26 00:37+0000\n"
"PO-Revision-Date: 2021-12-26 16:24+0100\n"
"Last-Translator: Antoni Bella Pérez <antonibella5@yahoo.com>\n"
"Language-Team: Catalan <kde-i18n-ca@kde.org>\n"
"Language: ca\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-Generator: Lokalize 21.12.0\n"

#: ../../glossary/useful_information/the_library.rst:13
msgid "The library: copy paste between projects"
msgstr "La biblioteca: copiar i enganxar entre projectes"

#: ../../glossary/useful_information/the_library.rst:17
msgid ""
"The **Library** is Kdenlive’s way to copy and paste sets of clips and "
"transitions between different projects. As its name already hints at: it is "
"not just a clipboard, but instead it is a library for all the things you in "
"more than a single project. The Library came to life around Kdenlive 15.04 "
"or so. Let’s right dive into **how to use the library** in your daily "
"Kdenlive workflow!"
msgstr ""
"La **Biblioteca** és la manera del Kdenlive de copiar i enganxar conjunts de "
"clips i transicions entre diferents projectes. Com el seu nom ja suggereix: "
"no és només un porta-retalls, sinó que és una biblioteca per a totes les "
"coses que es troben en més d'un únic projecte. La Biblioteca va néixer al "
"voltant del Kdenlive 15.04 aproximadament. Fem una immersió directa a **com "
"utilitzar la biblioteca** en el vostre flux de treball diari del Kdenlive!"

#: ../../glossary/useful_information/the_library.rst:20
msgid "Step 1: Copy Stuff to Your Library"
msgstr "Pas 1: Copiar el material a la biblioteca"

#: ../../glossary/useful_information/the_library.rst:29
msgid ""
"The **Library** is Kdenlive’s central place **for copying and pasting "
"between projects**."
msgstr ""
"La **Biblioteca** és el lloc central del Kdenlive **per copiar i enganxar "
"entre projectes**."

#: ../../glossary/useful_information/the_library.rst:31
msgid ""
"If you don’t see yet the library pane, make sure to display using :"
"menuselection:`View --> Library`."
msgstr ""
"Si encara no veieu la subfinestra de la biblioteca, assegureu-vos de mostrar-"
"la utilitzant :menuselection:`Visualitza --> Biblioteca`."

#: ../../glossary/useful_information/the_library.rst:35
msgid ""
"The library should look slightly familiar, as it looks similar to the "
"project bin. However, while the project bin changes with each project, the "
"library is independent of your projects. It’s always the same library, "
"there’s only one of it."
msgstr ""
"La biblioteca hauria de semblar-vos lleugerament familiar, ja que és similar "
"a la safata del projecte. No obstant això, mentre que la safata del projecte "
"canvia amb cada projecte, la biblioteca és independent dels vostres "
"projectes. Sempre és la mateixa biblioteca, només n'hi ha una."

#: ../../glossary/useful_information/the_library.rst:39
msgid ""
"A quite useful Kdenlive window layout is to group the library together with "
"the project bin, and optionally the project notes. This way, you don’t need "
"extra screen space for the library, yet it is no further away than just a "
"single click."
msgstr ""
"Una disposició de finestres del Kdenlive força útil és agrupar la biblioteca "
"juntament amb la safata del projecte, i opcionalment les notes del projecte. "
"D'aquesta manera, no es necessita espai de pantalla extra per a la "
"biblioteca, i no és més lluny que un únic clic."

#: ../../glossary/useful_information/the_library.rst:48
msgid ""
"Next, **select** some **timeline clips (together with their effects), as "
"well as transitions, in the timeline**. You can load an existing project and "
"select some timeline clips and transitions at any time in order to copy it "
"into your library. There is no separate import. Simply load a project or "
"create a new one."
msgstr ""
"A continuació, **seleccioneu** alguns clips de línia de temps (juntament amb "
"els seus efectes), així com transicions, en la línia de temps**. Podeu "
"carregar un projecte existent i seleccionar alguns clips de línia de temps i "
"transicions en qualsevol moment per copiar-lo a la vostra biblioteca. No hi "
"ha cap importació separada. Carregueu un projecte o creeu-ne un de nou."

#: ../../glossary/useful_information/the_library.rst:57
msgid ""
"Switch to the library pane, if you haven’t yet done so. Then press **the "
"bookmark** button (Add Timeline Selection to Library), which is located at "
"the bottom of the library pane. It’s the third button from the left."
msgstr ""
"Canvieu a la subfinestra de la biblioteca, si encara no ho heu fet. Després "
"premeu el botó **adreça d'interès** (Afegeix una selecció de la línia de "
"temps a la biblioteca), que es troba a la part inferior de la subfinestra de "
"la biblioteca. És el tercer botó de l'esquerra."

#: ../../glossary/useful_information/the_library.rst:66
msgid ""
"Kdenlive now asks you to **name your new library item**. Give it some name, "
"and click :guilabel:`OK` to copy the selected timeline clips (with effects) "
"and transitions into your library."
msgstr ""
"El Kdenlive ara us demana que **anomeneu l'element de la biblioteca nova**. "
"Doneu-li un nom i feu clic a :guilabel:`D'acord` per copiar els clips de la "
"línia de temps seleccionats (amb efectes) i transicions a la vostra "
"biblioteca."

#: ../../glossary/useful_information/the_library.rst:68
msgid ""
"**Please note**: at this time, the names of library item need to be valid "
"filenames."
msgstr ""
"**Tingueu en compte**: en aquest moment, els noms dels elements de la "
"biblioteca han de ser noms vàlids de fitxer."

#: ../../glossary/useful_information/the_library.rst:77
msgid ""
"Of course, you can even **neatly organize** your library items **into "
"folders**, and subfolders. This is similar to what you may have come to "
"known from Kdenlive’s (project) bin, where you also can use folders to "
"organize your project (source) clips."
msgstr ""
"Per descomptat, fins i tot podeu **organitzar adequadament** els elements de "
"la biblioteca **en carpetes** i subcarpetes. Això és similar al que heu "
"conegut de la safata (de projecte) del Kdenlive, on també es poden utilitzar "
"carpetes per organitzar els clips del projecte (font)."

#: ../../glossary/useful_information/the_library.rst:79
msgid ""
"Use the :guilabel:`Add Folder` button at the bottom of the library to create "
"a new folder. You can rearrange library items and folders at any time by "
"simple dragging them into their new place."
msgstr ""
"Utilitzeu el botó :guilabel:`Afegeix una carpeta` a la part inferior de la "
"biblioteca per crear una carpeta nova. Podeu reorganitzar els elements de la "
"biblioteca i les carpetes en qualsevol moment arrossegant-los al seu lloc "
"nou."

#: ../../glossary/useful_information/the_library.rst:84
msgid "Step 2: Paste Library Item into (New) Project"
msgstr "Pas 2: Enganxar l'element de la biblioteca al projecte (nou)"

#: ../../glossary/useful_information/the_library.rst:93
msgid ""
"Now switch to another Kdenlive project by loading it, or alternatively start "
"with a fresh project from scratch. Next, go to the library pane and **select "
"the library item** you want to paste into your project. Then, press the :"
"guilabel:`Add Clip to Project` button (up to Kdenlive 16.08.1 this is "
"instead the :guilabel:`+` button, located in the same place)."
msgstr ""
"Ara canvieu a un altre projecte Kdenlive carregant-lo, o alternativament "
"comenceu amb un projecte nou des de zero. A continuació, aneu a la "
"subfinestra de la biblioteca i **seleccioneu l'element de la biblioteca** "
"que voleu enganxar al vostre projecte. Després, premeu el botó :guilabel:"
"`Afegeix un clip al projecte` (fins al Kdenlive 16.08.1 aquest era el botó :"
"guilabel:`+`, situat al mateix lloc)."

#: ../../glossary/useful_information/the_library.rst:102
msgid "Your project bin now contains the new library item you’ve just added."
msgstr ""
"La safata del projecte ara conté l'element nou que acabeu d'afegir de la "
"biblioteca."

#: ../../glossary/useful_information/the_library.rst:104
msgid ""
"You can rename library items at any time: :menuselection:`right click --> "
"Rename Library Clip`."
msgstr ""
"Podeu canviar el nom dels elements de la biblioteca en qualsevol moment: :"
"menuselection:`clic dret --> Reanomena un clip de la biblioteca`."

#: ../../glossary/useful_information/the_library.rst:109
msgid "Step 3: Drag Library Item into Timeline"
msgstr "Pas 3: Arrossegar l'element de la biblioteca a la línia de temps"

#: ../../glossary/useful_information/the_library.rst:118
msgid ""
"The selected library item has now been added to your (project) bin. You’ll "
"see this by switching to the (project) bin pane. You still have only a "
"**single (library) clip** at this stage. You can now drag it into the "
"timeline, wherever you want."
msgstr ""
"L'element seleccionat de la biblioteca s'ha afegit a la safata (projecte). "
"Es veu canviat a la subfinestra de la safata (projecte). Encara només hi ha "
"un **clip únic (biblioteca)** en aquesta etapa. Ara podeu arrossegar-lo a la "
"línia de temps, on vulgueu."

#: ../../glossary/useful_information/the_library.rst:120
msgid ""
"**Please note**: you cannot directly drag a library item from the library "
"into your timeline. You always need to add it to your project bin first."
msgstr ""
"**Tingueu en compte**: no podeu arrossegar directament un element de la "
"biblioteca des de la biblioteca a la línia de temps. Sempre cal afegir-lo a "
"la safata del projecte."

#: ../../glossary/useful_information/the_library.rst:125
msgid "Step 4: Expand Library Clip"
msgstr "Pas 4: Expandir el clip de la biblioteca"

#: ../../glossary/useful_information/the_library.rst:134
msgid ""
"Often, you next want to edit the contents of a library clip after you’ve "
"placed it on the timeline. Expanding means that you want to break up a "
"library clip into its contents for further editing. So, simply **select the "
"library clip** in the timeline. Then choose :menuselection:`Timeline --> "
"Current Clip --> Expand Clip`."
msgstr ""
"Sovint, voldreu editar a continuació el contingut d'un clip de la biblioteca "
"després d'haver-lo posat en la línia de temps. Expandir significa que voleu "
"dividir un clip de la biblioteca en el seu contingut per a una edició "
"posterior. Així que simplement **seleccioneu el clip de la biblioteca** a la "
"línia de temps. Després trieu :menuselection:`Línia de temps --> Clip actual "
"--> Expandeix el clip`."

#: ../../glossary/useful_information/the_library.rst:143
msgid ""
"**Done!** You can now edit the expanded contents as you would edit any other "
"timeline content."
msgstr ""
"**Fet!** Ara podeu editar el contingut expandit com qualsevol altre "
"contingut que editaríeu a la línia de temps."

#: ../../glossary/useful_information/the_library.rst:152
msgid ""
"Kdenlive has expanded all the clips inside the library item into its own bin "
"folder. This bin folder has the same name as the library clip, but without "
"the .mlt extension."
msgstr ""
"El Kdenlive ha expandit tots els clips dins de l'element de la biblioteca a "
"la seva pròpia carpeta de la safata. Aquesta carpeta de la safata té el "
"mateix nom que el clip de la biblioteca, però sense l'extensió «.mlt»."

#: ../../glossary/useful_information/the_library.rst:154
msgid ""
"After successful expansion, you may now **remove the original library clip "
"from your bin**. It’s not needed anymore (as you can also tell from the "
"missing reference count)."
msgstr ""
"Després d'una expansió reeixida, ara podeu **eliminar el clip original de la "
"biblioteca de la safata**. Ja no és necessari (com també es pot veure des "
"del comptador de referències que manquen)."

#: ../../glossary/useful_information/the_library.rst:159
msgid "Clip Expansion Details"
msgstr "Detalls de l'expansió del clip"

#: ../../glossary/useful_information/the_library.rst:168
msgid ""
"Before Kdenlive 16.12.0, library clips will be expanded **from the bottom "
"up**; which may be counter-intuitive (depending on your point of view). "
"Here, bottom up means that in case a library clip contains multiple tracks, "
"then you need to place the library on a lower track so there is room above "
"for the clip to expand."
msgstr ""
"Abans del Kdenlive 16.12.0, els clips de la biblioteca s'expandien **des de "
"baix cap amunt**; que pot ser contraintuïtiu (depenent del vostre punt de "
"vista). Aquí, de baix cap amunt significa que en cas que un clip de la "
"biblioteca contingui múltiples pistes, llavors cal col·locar la biblioteca "
"en una pista més baixa, de manera que hi hagi espai per sobre per a què el "
"clip s'expandeixi."

#: ../../glossary/useful_information/the_library.rst:177
msgid ""
"From Kdenlive 16.12.0 on, the contents of the **library clip will be "
"expanded down** from the track where it has been placed on and below. That "
"is, place your library on an upper track with suitable room below."
msgstr ""
"A partir del Kdenlive 16.12.0, el contingut del **clip de biblioteca "
"s'expandeix cap avall** des de la pista on s'ha col·locat. És a dir, "
"col·loqueu la vostra biblioteca en una pista superior amb un espai adequat a "
"sota."

#: ../../glossary/useful_information/the_library.rst:179
msgid ""
"Anyway, if there aren’t enough tracks below the library clip, yet there are "
"enough tracks in the timeline, then Kdenlive will attempt to **shuffle the "
"library clip up** a number of tracks, before then expanding it."
msgstr ""
"De totes maneres, si no hi ha prou pistes per sota del clip de la "
"biblioteca, encara que hi hagi suficients pistes en la línia de temps, "
"llavors el Kdenlive intentarà **barrejar el clip de la biblioteca amunt** un "
"nombre de pistes, abans d'expandir-la."

#: ../../glossary/useful_information/the_library.rst:183
msgid ""
"In any case, to expand a library clip into its contents, you’ll always need "
"**necessary free space in the timeline**. This means that there cannot be "
"any clips or transitions within the start and end of the library clip on as "
"many  adjacent tracks as to be needed when expanding a multi-track library "
"item. Simply put: just make sure that the library item has room to expand, "
"otherwise there can be other clips and transitions above and below the "
"library clip, they just need to be out of the area of expansion."
msgstr ""
"En qualsevol cas, per expandir un clip de la biblioteca al seu contingut, "
"sempre necessitareu **espai lliure necessari en la línia de temps**. Això "
"vol dir que no pot haver-hi clips o transicions dins de l'inici i el final "
"del clip de la biblioteca en tantes pistes adjacents que es necessiten quan "
"s'expandeix un element de la biblioteca multipista. Senzillament, assegureu-"
"vos que l'element de la biblioteca tingui espai per expandir-se, en cas "
"contrari pot haver-hi altres clips i transicions per sobre i per sota del "
"clip de la biblioteca, senzillament cal que estiguin fora de l'àrea "
"d'expansió."

#: ../../glossary/useful_information/the_library.rst:192
msgid ""
"**Please note**: starting with Kdenlive 16.12.0, you can also **expand a "
"library immediately below a transition**; that is, the library clip is on "
"the next lower track in the timeline. This is useful for such cases where "
"you, for instance, have a clip running the full length of your project on "
"the topmost track and showing your company logo, channel logo, or something "
"similar. If you then use an explicit transition added to this clip over "
"compositing, you can still correctly expand the library clip on the second-"
"topmost track."
msgstr ""
"**Tingueu en compte**: començant amb el Kdenlive 16.12.0, també podeu "
"**expandir una biblioteca immediatament per sota d'una transició**; és a "
"dir, el clip de la biblioteca es troba a la pista immediatament a sota a la "
"línia de temps. Això és útil per a casos en els quals, per exemple, es té un "
"clip que té tota la longitud del projecte a la pista superior i que mostra "
"el logotip de la seva empresa, el logotip del canal o alguna cosa similar. "
"Si llavors utilitzeu una transició explícita afegida a aquest clip sobre la "
"composició, encara podreu expandir correctament el clip de la biblioteca en "
"la segona pista més alta."

#: ../../glossary/useful_information/the_library.rst:197
msgid "Configuring Your Library Storage Location"
msgstr "Configuració de la ubicació d'emmagatzematge de la biblioteca"

# skip-rule: t-acc_obe
#: ../../glossary/useful_information/the_library.rst:206
msgid ""
"All items in your Kdenlive library are stored in a user-configurable place "
"inside your file system. The default location, unless configured otherwise, "
"is where your other semi-temporary caching data is stored. Typically, this "
"is `$HOME/.cache/kdenlive/library`. Your library clips are then stored "
"inside this directory, as well as in subdirectories in case you also use "
"library folders."
msgstr ""
"Tots els elements de la biblioteca del Kdenlive s'emmagatzemen en un lloc "
"configurable per l'usuari dins del vostre sistema de fitxers. La ubicació "
"predeterminada, llevat que estigui configurada d'una altra manera, és on "
"s'emmagatzemen les altres dades d'emmagatzematge de la memòria cau "
"semitemporal. Normalment, això és `$HOME/.cache/kdenlive/library`. Els clips "
"de la biblioteca s'emmagatzemen dins d'aquest directori, així com en els "
"subdirectoris en cas que també utilitzeu carpetes de la biblioteca."

#: ../../glossary/useful_information/the_library.rst:208
msgid ""
"To change the location of your library, go to :menuselection:`Settings --> "
"Configure Kdenlive`. Next, select the section :guilabel:`Environment`. "
"Switch to the :guilabel:`Default folders` tab. Locate the part titled :"
"guilabel:`Library folder`, and deselect the option :guilabel:`Use default "
"folder`. Select or enter another location for your Kdenlive library."
msgstr ""
"Per canviar la ubicació de la vostra biblioteca, aneu a :menuselection:"
"`Arranjament --> Configura el Kdenlive`. A continuació, seleccioneu la "
"secció :guilabel:`Entorn`. Canvieu a la pestanya :guilabel:`Carpetes "
"predeterminades`. Localitzeu la part titulada :guilabel:`Carpeta de la "
"biblioteca`, i desseleccioneu l'opció :guilabel:`Usa la carpeta "
"predeterminada`. Seleccioneu o introduïu una altra ubicació per a la vostra "
"biblioteca del Kdenlive."

#: ../../glossary/useful_information/the_library.rst:212
msgid ""
"Please note: Kdenlive won’t move existing library files to the new location "
"you’ve set. You’ll need to do this manually using a file browser or from the "
"command line."
msgstr ""
"Tingueu en compte que el Kdenlive no mourà els fitxers de la biblioteca "
"existents a la nova ubicació que heu establert. Haureu de fer-ho manualment "
"utilitzant un navegador de fitxers o des de la línia d'ordres."
