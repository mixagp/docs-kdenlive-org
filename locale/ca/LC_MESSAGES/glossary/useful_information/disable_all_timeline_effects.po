# Translation of docs_kdenlive_org_glossary___useful_information___disable_all_timeline_effects.po to Catalan
# Copyright (C) 2021 This_file_is_part_of_KDE
# Licensed under the <a href="https://creativecommons.org/licenses/by-sa/4.0/">Creative Commons License SA 4.0</a> unless stated otherwise
# This file is distributed under the same license as the Kdenlive Manual package.
#
# Josep M. Ferrer <txemaq@gmail.com>, 2021.
msgid ""
msgstr ""
"Project-Id-Version: documentation-docs-kdenlive-org\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2021-11-15 00:22+0000\n"
"PO-Revision-Date: 2021-12-19 19:08+0100\n"
"Last-Translator: Josep M. Ferrer <txemaq@gmail.com>\n"
"Language-Team: Catalan <kde-i18n-ca@kde.org>\n"
"Language: ca\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-Generator: Lokalize 20.12.0\n"

#: ../../glossary/useful_information/disable_all_timeline_effects.rst:13
msgid "Disable All Timeline Effects"
msgstr "Desactivar tots els efectes de la línia de temps"

#: ../../glossary/useful_information/disable_all_timeline_effects.rst:15
msgid ""
"Did you know that you can **temporarily disable all timeline effects at once?"
"** This may be helpful when you want to do some timeline work, yet some "
"performance heavy effects slow down this work. Alternatively, you may want "
"to consider using Kdenlive’s timeline preview."
msgstr ""
"Sabíeu que podeu **desactivar temporalment tots els efectes de la línia de "
"temps alhora?** Això pot ser útil quan voleu fer algun treball a la línia de "
"temps, però alguns efectes de rendiment pesat alenteixen aquest treball. "
"Alternativament, és possible que vulgueu considerar utilitzar la "
"previsualització de la línia de temps del Kdenlive."

#: ../../glossary/useful_information/disable_all_timeline_effects.rst:24
msgid ""
"You’ll find the corresponding option in the main menu :menuselection:"
"`Timeline --> Disable Timeline Effects`. This disables or re-enables all "
"timeline effects, that is, timeline clip effects and track effects."
msgstr ""
"Trobareu l'opció corresponent en el menú principal :menuselection:`Línia de "
"temps --> Desactiva els efectes de la línia de temps`. Això desactiva o "
"reactiva tots els efectes de la línia de temps, és a dir, els efectes de "
"clip de la línia de temps i els efectes de la pista."

#: ../../glossary/useful_information/disable_all_timeline_effects.rst:26
msgid ""
"However, please note that prior to Kdenlive 16.08.1, track effects are not "
"properly disabled or re-enabled by :menuselection:`Timeline --> Disable "
"Timeline Effects`."
msgstr ""
"No obstant això, tingueu en compte que abans del Kdenlive 16.08.1, els "
"efectes de la pista no estan correctament desactivats o reactivats a :"
"menuselection:`Línia de temps --> Desactiva els efectes de la línia de "
"temps`."

#: ../../glossary/useful_information/disable_all_timeline_effects.rst:28
msgid ""
"Please see :ref:`effects_everywhere` about how to temporarily disable bin "
"clip effects."
msgstr ""
"Si us plau, vegeu :ref:`effects_everywhere` sobre com desactivar "
"temporalment els efectes de clip de la safata."
