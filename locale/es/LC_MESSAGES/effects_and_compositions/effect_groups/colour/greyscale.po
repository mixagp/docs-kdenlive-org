# Spanish translations for docs_kdenlive_org_effects_and_compositions___effect_groups___colour___greyscale.po package.
# Copyright (C) licensed under the  <a href="https://creativecommons.org/licenses/by-sa/4.0/">Creative Commons License SA 4.0</a> unless stated otherwise
# This file is distributed under the same license as the Kdenlive Manual package.
#
# Automatically generated, 2021.
# Eloy Cuadra <ecuadra@eloihr.net>, 2021.
msgid ""
msgstr ""
"Project-Id-Version: "
"docs_kdenlive_org_effects_and_compositions___effect_groups___colour___greyscale\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2021-11-18 00:21+0000\n"
"PO-Revision-Date: 2021-11-24 18:17+0100\n"
"Last-Translator: Eloy Cuadra <ecuadra@eloihr.net>\n"
"Language-Team: Spanish <kde-l10n-es@kde.org>\n"
"Language: es\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-Generator: Lokalize 21.08.3\n"

#: ../../effects_and_compositions/effect_groups/colour/greyscale.rst:14
msgid "Greyscale"
msgstr "Escala de grises"

#: ../../effects_and_compositions/effect_groups/colour/greyscale.rst:16
msgid "Contents"
msgstr "Contenido"

#: ../../effects_and_compositions/effect_groups/colour/greyscale.rst:18
msgid ""
"This is the `Greyscale <https://www.mltframework.org/plugins/FilterGreyscale/"
">`_ MLT filter."
msgstr ""
"Este es el filtro `Greyscale <https://www.mltframework.org/plugins/"
"FilterGreyscale/>`_ de MLT."

#: ../../effects_and_compositions/effect_groups/colour/greyscale.rst:20
msgid "Convert colour image to greyscale."
msgstr "Convertir una imagen en color a escala de grises."

#: ../../effects_and_compositions/effect_groups/colour/greyscale.rst:22
msgid "https://youtu.be/5L9KTfJFw80"
msgstr "https://youtu.be/5L9KTfJFw80"

#: ../../effects_and_compositions/effect_groups/colour/greyscale.rst:24
msgid "https://youtu.be/bgNd2bHnvSY"
msgstr "https://youtu.be/bgNd2bHnvSY"

#~ msgid "Discards color information."
#~ msgstr "Descarta la información de color."
