# Spanish translations for docs_kdenlive_org_user_interface___menu___settings_menu___manage_project_profiles.po package.
# Copyright (C) licensed under the  <a href="https://creativecommons.org/licenses/by-sa/4.0/">Creative Commons License SA 4.0</a> unless stated otherwise
# This file is distributed under the same license as the Kdenlive Manual package.
#
# Automatically generated, 2021.
# Eloy Cuadra <ecuadra@eloihr.net>, 2021.
msgid ""
msgstr ""
"Project-Id-Version: "
"docs_kdenlive_org_user_interface___menu___settings_menu___manage_project_profiles\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2021-11-13 00:22+0000\n"
"PO-Revision-Date: 2021-11-14 04:56+0100\n"
"Last-Translator: Eloy Cuadra <ecuadra@eloihr.net>\n"
"Language-Team: Spanish <kde-l10n-es@kde.org>\n"
"Language: es\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-Generator: Lokalize 21.08.3\n"

#: ../../user_interface/menu/settings_menu/manage_project_profiles.rst:13
msgid "Manage Project Profiles"
msgstr ""

#: ../../user_interface/menu/settings_menu/manage_project_profiles.rst:16
msgid "Contents"
msgstr "Contenido"

#: ../../user_interface/menu/settings_menu/manage_project_profiles.rst:18
msgid "This is available from the **Settings** menu."
msgstr ""

#: ../../user_interface/menu/settings_menu/manage_project_profiles.rst:24
msgid "Once the dialog appears, select a profile to modify from the drop down."
msgstr ""

#: ../../user_interface/menu/settings_menu/manage_project_profiles.rst:26
msgid ""
"Next, click the button with a green plus on it.  This will make all the "
"*Properties* fields editable."
msgstr ""

#: ../../user_interface/menu/settings_menu/manage_project_profiles.rst:28
msgid ""
"Fill in the settings for your project profile, give it a new *Description* "
"and click the **OK** button."
msgstr ""

#: ../../user_interface/menu/settings_menu/manage_project_profiles.rst:30
msgid ""
"See also `HOWTO Produce 4k and 2K videos, YouTube compatible <https://forum."
"kde.org/viewtopic.php?f=272&amp;t=124869#p329129>`_"
msgstr ""
