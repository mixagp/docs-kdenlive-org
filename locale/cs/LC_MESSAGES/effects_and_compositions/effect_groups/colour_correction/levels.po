# Copyright (C) licensed under the  <a href="https://creativecommons.org/licenses/by-sa/4.0/">Creative Commons License SA 4.0</a> unless stated otherwise
# This file is distributed under the same license as the Kdenlive Manual package.
# Vit Pelcak <vpelcak@suse.cz>, 2022.
#
msgid ""
msgstr ""
"Project-Id-Version: Kdenlive Manual \n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2021-11-18 00:21+0000\n"
"PO-Revision-Date: 2022-01-18 09:42+0100\n"
"Last-Translator: Vit Pelcak <vpelcak@suse.cz>\n"
"Language-Team: Czech <kde-i18n-doc@kde.org>\n"
"Language: cs\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=3; plural=(n==1) ? 0 : (n>=2 && n<=4) ? 1 : 2;\n"
"X-Generator: Lokalize 21.12.1\n"

#: ../../effects_and_compositions/effect_groups/colour_correction/levels.rst:14
msgid "Levels"
msgstr "Úrovně"

#: ../../effects_and_compositions/effect_groups/colour_correction/levels.rst:16
msgid "Contents"
msgstr "Obsah"

#: ../../effects_and_compositions/effect_groups/colour_correction/levels.rst:18
msgid ""
"This is the `Frei0r levels <https://www.mltframework.org/plugins/"
"FilterFrei0r-levels/>`_ MLT filter."
msgstr ""
"Toto je `Frei0r levels <https://www.mltframework.org/plugins/FilterFrei0r-"
"levels/>`_ filtr MLT."

#: ../../effects_and_compositions/effect_groups/colour_correction/levels.rst:20
msgid "https://youtu.be/iMbohQnyFV4"
msgstr "https://youtu.be/iMbohQnyFV4"
