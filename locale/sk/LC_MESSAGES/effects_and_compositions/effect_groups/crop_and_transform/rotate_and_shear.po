# Copyright (C) licensed under the  <a href="https://creativecommons.org/licenses/by-sa/4.0/">Creative Commons License SA 4.0</a> unless stated otherwise
# This file is distributed under the same license as the Kdenlive Manual package.
# Roman Paholik <wizzardsk@gmail.com>, 2021.
msgid ""
msgstr ""
"Project-Id-Version: Kdenlive Manual\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2021-12-03 00:38+0000\n"
"PO-Revision-Date: 2021-11-17 11:24+0100\n"
"Last-Translator: Roman Paholik <wizzardsk@gmail.com>\n"
"Language-Team: Slovak <kde-sk@linux.sk>\n"
"Language: sk\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Generator: Lokalize 21.08.3\n"
"Plural-Forms: nplurals=3; plural=(n==1) ? 0 : (n>=2 && n<=4) ? 1 : 2;\n"

#: ../../effects_and_compositions/effect_groups/crop_and_transform/rotate_and_shear.rst:13
msgid "Rotate and Shear"
msgstr "Otočiť a skosiť"

#: ../../effects_and_compositions/effect_groups/crop_and_transform/rotate_and_shear.rst:15
msgid "Contents"
msgstr "Obsah"

#: ../../effects_and_compositions/effect_groups/crop_and_transform/rotate_and_shear.rst:17
msgid ""
"This would appear to be the `Affine <https://www.mltframework.org/plugins/"
"FilterAffine/>`_ MLT filter as defined in :file:`rotation.xml` (:file:`/usr/"
"share/kdenlive/effects/`)."
msgstr ""

#: ../../effects_and_compositions/effect_groups/crop_and_transform/rotate_and_shear.rst:19
#, fuzzy
#| msgid "Rotate clip in any 3 directions"
msgid "Rotate clip in any 3 directions."
msgstr "Otočiť klip v akýchkoľvek 3 smeroch"

#: ../../effects_and_compositions/effect_groups/crop_and_transform/rotate_and_shear.rst:21
msgid ""
"This screen shot shows settings for Rotate and Shear that can correct wide-"
"screen footage shot while holding the camera the wrong orientation."
msgstr ""

#: ../../effects_and_compositions/effect_groups/crop_and_transform/rotate_and_shear.rst:23
msgid ""
"Do a rotate X of 900 units (the units are in tenths of a degree). You can "
"also adjust the size with this effect."
msgstr ""

#: ../../effects_and_compositions/effect_groups/crop_and_transform/rotate_and_shear.rst:29
msgid "https://youtu.be/WadSGu05HAw"
msgstr "https://youtu.be/WadSGu05HAw"
