# Copyright (C) licensed under the  <a href="https://creativecommons.org/licenses/by-sa/4.0/">Creative Commons License SA 4.0</a> unless stated otherwise
# This file is distributed under the same license as the Kdenlive Manual package.
#
# Łukasz Wojniłowicz <lukasz.wojnilowicz@gmail.com>, 2022.
msgid ""
msgstr ""
"Project-Id-Version: Kdenlive Manual \n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2021-11-26 00:21+0000\n"
"PO-Revision-Date: 2022-02-21 19:37+0100\n"
"Last-Translator: Łukasz Wojniłowicz <lukasz.wojnilowicz@gmail.com>\n"
"Language-Team: Polish <kde-i18n-doc@kde.org>\n"
"Language: pl\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=3; plural=(n==1 ? 0 : n%10>=2 && n%10<=4 && (n%100<10 "
"|| n%100>=20) ? 1 : 2);\n"
"X-Generator: Lokalize 22.03.70\n"

#: ../../glossary/useful_information/howto_editing_audio_volume_with_keyframes.rst:13
msgid "How to editing audio volume with keyframes"
msgstr "Jak zmienić głośność dźwięku klatkami kluczowymi"

#: ../../glossary/useful_information/howto_editing_audio_volume_with_keyframes.rst:15
msgid ""
"In this *HOWTO* video we show you how to **edit the audio volume of a clip** "
"using keyframes: learn how to add a *Volume* effect to adjust the volume of "
"a clip, how to add and edit keyframes in the effect properties, and how to "
"manage and edit keyframes directly in the timeline. Double click to add a "
"keyframe, remove a keyframe by dragging it above or below the clip."
msgstr ""
"W tym *poradniku* pokazujemy jak **zmienić głośność dźwięku wycinka** "
"używając klatek kluczowych: dowiedz się jak dodać efekt *Głośności*, aby "
"dostosować głośność wycinka, jak dodawać i zmieniać klatki kluczowe we "
"właściwościach efektu oraz jak zarządzać i zmieniać klatki kluczowe "
"bezpośrednio na osi czasu. Naciśnij dwukrotnie, aby dodać klatkę kluczową. "
"Usunąć klatkę kluczową możesz przeciągając ją powyżej lub poniżej wycinka."
