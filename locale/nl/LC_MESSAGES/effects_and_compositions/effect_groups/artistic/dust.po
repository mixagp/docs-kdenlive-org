# Copyright (C) licensed under the  <a href="https://creativecommons.org/licenses/by-sa/4.0/">Creative Commons License SA 4.0</a> unless stated otherwise
# This file is distributed under the same license as the Kdenlive Manual package.
#
# Freek de Kruijf <freekdekruijf@kde.nl>, 2021.
msgid ""
msgstr ""
"Project-Id-Version: Kdenlive Manual \n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2021-11-18 00:21+0000\n"
"PO-Revision-Date: 2021-11-18 11:11+0100\n"
"Last-Translator: Freek de Kruijf <freekdekruijf@kde.nl>\n"
"Language-Team: \n"
"Language: nl\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-Generator: Lokalize 21.08.3\n"

#: ../../effects_and_compositions/effect_groups/artistic/dust.rst:14
msgid "Dust"
msgstr "Stof"

#: ../../effects_and_compositions/effect_groups/artistic/dust.rst:16
msgid "Contents"
msgstr "Inhoud"

#: ../../effects_and_compositions/effect_groups/artistic/dust.rst:18
msgid ""
"See `dust filter <https://www.mltframework.org/plugins/FilterDust/>`_ from "
"MLT."
msgstr ""
"Zie `stoffilter <http://www.mltframework.org/bin/view/MLT/FilterDust>`_ uit "
"MLT."

#: ../../effects_and_compositions/effect_groups/artistic/dust.rst:20
msgid "Add dust and specks to the video clip, as in old movies."
msgstr "Stof en spikkels aan de videoclip toevoegen, net als in oude films."

#: ../../effects_and_compositions/effect_groups/artistic/dust.rst:22
msgid "https://youtu.be/h0s0PBfpcEE"
msgstr "https://youtu.be/h0s0PBfpcEE"

#: ../../effects_and_compositions/effect_groups/artistic/dust.rst:24
msgid "https://youtu.be/wbX7Df8rC0M"
msgstr "https://youtu.be/wbX7Df8rC0M"
