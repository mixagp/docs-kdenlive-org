# Copyright (C) licensed under the  <a href="https://creativecommons.org/licenses/by-sa/4.0/">Creative Commons License SA 4.0</a> unless stated otherwise
# This file is distributed under the same license as the Kdenlive Manual package.
#
# Freek de Kruijf <freekdekruijf@kde.nl>, 2021.
msgid ""
msgstr ""
"Project-Id-Version: Kdenlive Manual \n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2021-11-28 00:22+0000\n"
"PO-Revision-Date: 2021-11-28 23:52+0100\n"
"Last-Translator: Freek de Kruijf <freekdekruijf@kde.nl>\n"
"Language-Team: \n"
"Language: nl\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-Generator: Lokalize 21.08.3\n"

#: ../../user_interface/menu/settings_menu/configure_shortcuts.rst:14
msgid "Configure Shortcuts"
msgstr "Sneltoetsen configureren"

#: ../../user_interface/menu/settings_menu/configure_shortcuts.rst:17
msgid "Contents"
msgstr "Inhoud"

#: ../../user_interface/menu/settings_menu/configure_shortcuts.rst:19
msgid ""
"Allows modification of the keyboard shortcuts for various **Kdenlive** tasks."
msgstr ""
"Biedt wijzigen van de sneltoetsen voor verschillende taken van **Kdenlive**."

#: ../../user_interface/menu/settings_menu/configure_shortcuts.rst:26
msgid "Raise widgets with shortcuts (window)"
msgstr "Widgets omhoog brengen met sneltoetsen (venster)"

#: ../../user_interface/menu/settings_menu/configure_shortcuts.rst:31
msgid ""
"With the word \"raise\" you see all dock widgets (window) on which you can "
"assign shortcuts to raise dock widgets (window). The actions are called like "
"\"Raise Project Bin\"."
msgstr ""
"Met \"omhoog brengen\" ziet u alle vastzetwidgets (vensters) waaraan u "
"sneltoetsen kunt toekennen om vastzetwidgets (vensters) omhoog te brengen. "
"De acties worden genoemd zoiets als \"Project-bin omhoog brengen\"."

#: ../../user_interface/menu/settings_menu/configure_shortcuts.rst:39
msgid "Shortcuts for keyframe functions"
msgstr "Sneltoetsen voor keyframefuncties"

#: ../../user_interface/menu/settings_menu/configure_shortcuts.rst:44
msgid ""
"Allow assigning shortcuts to 3 keyframe functions: *Add/Remove Keyframe*, "
"*Go to next keyframe* and *Go to previous keyframe*. Click on the clip and "
"you can add/remove keyframe by shortcut."
msgstr ""
"Biedt het toekennen van sneltoetsen aan 3 keyframefuncties: *Keyframe "
"toevoegen/verwijderen*, *Ga naar volgende keyframe* en *Ga naar vorige "
"keyframe*. Klik op de clip en u kunt een keyframe toevoegen/verwijderen met "
"een sneltoets."

#: ../../user_interface/menu/settings_menu/configure_shortcuts.rst:53
msgid "Command Bar"
msgstr "Commandobalk"

#: ../../user_interface/menu/settings_menu/configure_shortcuts.rst:58
msgid ""
"The command bar allows to easily searching for any action in Kdenlive like "
"changing themes, adding effects, opening files and more. It can be accessed "
"with the shortcut: Ctrl + Alt + i. The shortcut is defined by KDE-Framework, "
"so do not change it. (This feature requires KDE Frameworks lib version 5.83)."
msgstr ""
"De commandobalk biedt het gemakkelijk zoeken naar elke actie in Kdenlive "
"zoals thema's wijzigen, effecten toevoegen, bestanden openen en meer. Er is "
"toegang toe met de sneltoets: Ctrl + Alt + i. De sneltoets is gedefinieerd "
"door KDE-Framework, wijzig deze dus niet. (Deze functie vereist KDE "
"Frameworks bibliotheekversie 5.83)."
