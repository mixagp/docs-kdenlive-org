# Xavier Besnard <xavier.besnard@neuf.fr>, 2022.
msgid ""
msgstr ""
"Project-Id-Version: Kdenlive Manual \n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2021-12-19 00:37+0000\n"
"PO-Revision-Date: 2022-01-20 11:34+0100\n"
"Last-Translator: Xavier Besnard <xavier.besnard@neuf.fr>\n"
"Language-Team: French <kde-francophone@kde.org>\n"
"Language: fr\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=(n > 1);\n"
"X-Generator: Lokalize 21.12.1\n"

#: ../../glossary/useful_information/insert_overwrite_advanced_timeline_editing.rst:13
msgid "Insert and Overwrite: advanced timeline editing"
msgstr ""
"Insertion et écrasement : modification avancée de la frise chronologique"

#: ../../glossary/useful_information/insert_overwrite_advanced_timeline_editing.rst:17
msgid ""
"Kdenlive offers advanced timeline editing functions. In this article we’re "
"looking at the :menuselection:`insert` |timeline-insert| and :menuselection:"
"`overwrite` |timeline-overwrite| advanced editing functions. A later article "
"then will cover the opposite :menuselection:`lift` |timeline-lift| and :"
"menuselection:`extract` |timeline-extract| functions."
msgstr ""
"Kdenlive offre des fonctions avancées d'édition de la frise chronologique. "
"Cet article est consacré aux fonctions d'édition avancées :menuselection:"
"`insérer` |timeline-insert| et :menuselection:`écraser` |timeline-"
"overwrite|. Ensuite, un prochain article couvrira les fonctions opposées :"
"menuselection:`améliorer` |timeline-lift| et :menuselection:`extraire` |"
"timeline-extract|."

#: ../../glossary/useful_information/insert_overwrite_advanced_timeline_editing.rst:19
msgid ""
"When inserting or overwriting some part in the timeline with some part from "
"a clip, we now face two zones, so how does this work out at all? We only "
"want to deal with three points, that is, with one zone and a point (for that "
"reason this is also sometimes termed three point editing). In consequence, "
"there are two different insert/overwrite operations:"
msgstr ""
"Lors de l'insertion ou de l'écrasement d'une partie de la frise "
"chronologique avec une partie d'une vidéo, nous sommes maintenant confrontés "
"à deux zones. Ainsi, comment cela fonctionne-t-il ? Nous voulons seulement "
"traiter trois points, c'est-à-dire une zone et un point (Pour cette raison, "
"ceci est parfois appelé « Montage en trois points »). Par conséquent, il "
"existe deux opérations d'insertion / écrasement différentes :"

#: ../../glossary/useful_information/insert_overwrite_advanced_timeline_editing.rst:21
msgid ""
"insert/overwrite a clip zone into timeline at some point (cursor/playhead), "
"or"
msgstr ""
"insérer / écraser une zone de vidéo dans la frise chronologique à un point "
"donné (curseur / tête de lecture), ou"

#: ../../glossary/useful_information/insert_overwrite_advanced_timeline_editing.rst:22
msgid "insert/overwrite a clip starting at some point into a timeline zone."
msgstr ""
"insérer / écraser une vidéo en commençant à un certain point dans une zone "
"de la frise chronologique."

#: ../../glossary/useful_information/insert_overwrite_advanced_timeline_editing.rst:25
msgid "Insert Clip Zone into Timeline at Timeline Cursor"
msgstr ""
"Insérer une zone de vidéo dans la frise chronologique à partir de la "
"position du curseur"

#: ../../glossary/useful_information/insert_overwrite_advanced_timeline_editing.rst:35
msgid ""
"As we’re going to insert a clip zone into the timeline, first make sure that "
"the :menuselection:`switch for using timeline zone is crossed out` |timeline-"
"use-zone-off| (it’s *off*). This is also shown in the screenshot. (You’ll "
"find this switch above the track headers, next to the track size zoom in/out "
"controls.)"
msgstr ""
"Comme nous allons insérer une zone de vidéo dans la frise chronologique, "
"veuillez d'abord vous assurer que le :menuselection:`commutateur pour "
"utiliser la zone de la frise chronologique est cochée` |timeline-use-zone-"
"off| (elle est *décoché*). Ceci est également présenté dans la copie "
"d'écran. (Vous trouverez ce commutateur au-dessus des en-têtes de piste, à "
"côté des commandes de zoom avant / arrière concernant la taille de la piste)."

#: ../../glossary/useful_information/insert_overwrite_advanced_timeline_editing.rst:37
msgid ""
"A visual clue (albeit a rather unintrusive one) is that the **timeline zone "
"bar** is now *dimmed*."
msgstr ""
"Un indice visuel (bien que non intrusif) est que la **barre de la zone de la "
"frise chronologique** est maintenant *estompé*."

#: ../../glossary/useful_information/insert_overwrite_advanced_timeline_editing.rst:46
msgid ""
"Next, mark the **clip region** of the source clip you want to insert into "
"the timeline. You do this as usual, using either the :kbd:`I` and :kbd:`O` "
"shortcuts, or the set zone in/out buttons of the clip monitor."
msgstr ""
"Ensuite, marquez la **région de vidéo** dans la vidéo source que vous voulez "
"insérer dans la frise chronologique. Vous faites cela comme d'habitude, en "
"utilisant soit les raccourcis :kbd:`I` et :kbd:`O`, soit les boutons de zone "
"d'entrée / sortie sur l'écran des vidéos."

#: ../../glossary/useful_information/insert_overwrite_advanced_timeline_editing.rst:55
msgid ""
"Now **place the timeline cursor** to where you want to start with the insert."
msgstr ""
"Maintenant **placez le curseur de la frise chronologique** à l’endroit où "
"vous voulez commencer l’insertion."

#: ../../glossary/useful_information/insert_overwrite_advanced_timeline_editing.rst:57
msgid ""
"Also make sure to **select the correct track** for insertion – using the :"
"kbd:`cursor up` and :kbd:`cursor down` keys. (Remember that the currently "
"selected track is marked with the semi-transparent selection color, the "
"exact color of which depends on your particular color theme.)"
msgstr ""
"Veuillez aussi vous assurer de **sélectionner la bonne piste** pour "
"l'insertion - en utilisant les touches :kbd:`flèche vers le haut` et :kbd:"
"`flèche vers le bas`. (Rappelez-vous que la piste actuellement sélectionnée "
"est marquée par la couleur de sélection semi-transparente, dont la couleur "
"exacte dépend de votre thème particulier de couleurs)."

#: ../../glossary/useful_information/insert_overwrite_advanced_timeline_editing.rst:66
msgid ""
"Finally press the :kbd:`v` shortcut, or click the :menuselection:`insert "
"clip zone toolbar button` |timeline-insert|, or use :menuselection:`Timeline "
"--> Insertion --> Insert Clip Zone in Timeline`."
msgstr ""
"Enfin, appuyez sur le raccourci :kbd:`v’ ou cliquez sur le :menuselection:"
"`bouton « Insérer une zone vidéo » dans la barre d’outils`|frise-"
"chronologique-insérer| ou utiliser :menuselection:`Frise chronologique / "
"Insertion /Insérer une zone de vidéo dans la frise chronologique”."

#: ../../glossary/useful_information/insert_overwrite_advanced_timeline_editing.rst:72
msgid ""
"Insertion starts from the timeline cursor, and not from the timeline zone "
"start (because we chose to ignore it in our very first step)."
msgstr ""
"L’insertion débute à partir du curseur de la frise chronologique et non à "
"partir du début de la zone de la frise chronologique (parce que nous avons "
"choisi de l’ignorer dans notre toute première étape)."

#: ../../glossary/useful_information/insert_overwrite_advanced_timeline_editing.rst:73
#: ../../glossary/useful_information/insert_overwrite_advanced_timeline_editing.rst:124
msgid "Locked tracks are unaffected, such as the topmost track in our example."
msgstr ""
"Les pistes verrouillées ne sont pas affectées, comme la piste la plus haute "
"de notre exemple."

#: ../../glossary/useful_information/insert_overwrite_advanced_timeline_editing.rst:74
#: ../../glossary/useful_information/insert_overwrite_advanced_timeline_editing.rst:125
msgid ""
"Unlocked tracks get affected in that whatever is at the insertion point and "
"later in the timeline gets shifted away to make room for the insertion."
msgstr ""
"Les pistes déverrouillées sont affectées quelque soit ce qui est présent au "
"point d’insertion et ce qui existe plus tard dans la frise chronologique est "
"décalé pour faire de la place pour l’insertion."

#: ../../glossary/useful_information/insert_overwrite_advanced_timeline_editing.rst:77
msgid "Insert Clip (from In Point) into Timeline Zone"
msgstr ""
"Insérer une vidéo (à partir du point d’entrée) dans la zone de la frise "
"chronologique"

#: ../../glossary/useful_information/insert_overwrite_advanced_timeline_editing.rst:86
msgid ""
"This time, we’re going to insert some part of a clip to *exactly fit* into "
"the timeline zone. So we now need to switch on using the :menuselection:"
"`timeline zone` |timeline-use-zone-on|. This is also shown in the "
"screenshot. (You’ll find this switch above the track headers, next to the "
"track size zoom in/out controls.)"
msgstr ""
"Cette fois, nous allons insérer une partie d'une vidéo pour qu'elle s'insère "
"*exactement* dans la zone de frise chronologique. L'option :menuselection:"
"`zone de la frise chronologique` |timeline-use-zone-on| doit être utilisée. "
"Celle-ci est également présentée dans la copie d'écran. (Vous trouverez ce "
"commutateur au-dessus des en-têtes de piste, à côté des commandes de zoom "
"avant / arrière concernant la taille de la piste)."

#: ../../glossary/useful_information/insert_overwrite_advanced_timeline_editing.rst:88
msgid ""
"A visual clue (albeit a rather unintrusive one) is that the **timeline zone "
"bar** is now *bright*."
msgstr ""
"Un indice visuel (bien que non intrusif) est que la **barre de la zone de la "
"frise chronologique** est maintenant *brillant*."

#: ../../glossary/useful_information/insert_overwrite_advanced_timeline_editing.rst:97
msgid ""
"This time, we only need to **set the in point** for our source clip. The out "
"point doesn’t matter, as it will be later determined automatically by the "
"length of the timeline zone."
msgstr ""
"Cette fois, nous n’avons qu’à **définir le point d’entrée** pour notre vidéo "
"source. Le point de sortie n’a pas d’importance, car il sera déterminé plus "
"tard automatiquement par la longueur de la zone de la frise chronologique."

#: ../../glossary/useful_information/insert_overwrite_advanced_timeline_editing.rst:106
msgid ""
"Now, place **mark the timeline zone** into which you want to insert a part "
"of your source clip. Notice that the timeline cursor position now doesn’t "
"matter."
msgstr ""
"Maintenant, placez **une marque dans la zone de la frise chronologique** à "
"laquelle vous voulez insérer une partie de votre vidéo source. Veuillez "
"noter que la position du curseur de la frise chronologique n'a aucune "
"importance."

#: ../../glossary/useful_information/insert_overwrite_advanced_timeline_editing.rst:108
msgid ""
"Make sure to **select the correct track** for insertion – using the :kbd:"
"`cursor up` and :kbd:`cursor down` keys."
msgstr ""
"Veuillez vous assurer de **sélectionner la piste correcte** pour l’insertion "
"– en utilisant les touches :kbd:‘flèche vers le haut’ et :kbd:‘flèche vers "
"le bas’."

#: ../../glossary/useful_information/insert_overwrite_advanced_timeline_editing.rst:117
msgid ""
"Finally press the :kbd:`V` shortcut, or click the :menuselection:`insert "
"clip zone toolbar button` |timeline-insert|, or use :menuselection:`Timeline "
"--> Insertion --> Insert Clip Zone in Timeline`."
msgstr ""
"Enfin, appuyez sur le raccourci :kbd:`v’ ou cliquez sur le :menuselection:"
"`bouton « Insérer une zone vidéo » dans la barre d’outils`|frise-"
"chronologique-insérer| ou utiliser :menuselection:`Frise chronologique / "
"Insertion /Insérer une zone de vidéo dans la frise chronologique”."

#: ../../glossary/useful_information/insert_overwrite_advanced_timeline_editing.rst:123
msgid ""
"Insertion starts from the beginning of the timeline zone, and not from the "
"timeline cursor position (because we chose to enable the timeline zone in "
"our very first step)."
msgstr ""
"L’insertion débute au début de la zone de la frise chronologique et non à "
"partir de la position du curseur dans celle-ci (parce que nous avons choisi "
"d’activer la zone de la frise chronologique dans notre toute première étape)."

#: ../../glossary/useful_information/insert_overwrite_advanced_timeline_editing.rst:129
msgid "Overwrite Timeline with Clip Zone"
msgstr "Écraser la frise chronologique avec une zone de vidéo"

#: ../../glossary/useful_information/insert_overwrite_advanced_timeline_editing.rst:131
#: ../../glossary/useful_information/insert_overwrite_advanced_timeline_editing.rst:138
msgid ":menuselection:`overwrite` |timeline-overwrite|"
msgstr ":menuselection:`écraser` |frise-chronologique-écraser|"

#: ../../glossary/useful_information/insert_overwrite_advanced_timeline_editing.rst:133
#: ../../glossary/useful_information/insert_overwrite_advanced_timeline_editing.rst:140
msgid "(will be documented later)"
msgstr "(sera documenté plus tard)"

#: ../../glossary/useful_information/insert_overwrite_advanced_timeline_editing.rst:136
msgid "Overwrite Timeline Zone with Clip"
msgstr "Écraser une zone de la frise chronologique avec une vidéo"
