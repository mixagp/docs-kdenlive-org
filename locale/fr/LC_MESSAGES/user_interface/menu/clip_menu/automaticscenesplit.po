# Xavier Besnard <xavier.besnard@neuf.fr>, 2021, 2022.
msgid ""
msgstr ""
"Project-Id-Version: Kdenlive Manual \n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2021-12-21 00:37+0000\n"
"PO-Revision-Date: 2022-01-03 14:07+0100\n"
"Last-Translator: Xavier Besnard <xavier.besnard@neuf.fr>\n"
"Language-Team: French <kde-francophone@kde.org>\n"
"Language: fr\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=(n > 1);\n"
"X-Generator: Lokalize 21.12.0\n"

#: ../../user_interface/menu/clip_menu/automaticscenesplit.rst:14
msgid "Automatic Scene Split"
msgstr "Découpage automatique de scènes"

#: ../../user_interface/menu/clip_menu/automaticscenesplit.rst:16
msgid "Contents"
msgstr "Contenu"

#: ../../user_interface/menu/clip_menu/automaticscenesplit.rst:18
msgid ""
"This job detects Scene changes in the clip and create markers or cut the "
"clip into sub clips."
msgstr ""
"Cette tâche détecte les changements de scène dans la vidéo et crée des "
"marqueurs ou découpe la vidéo en sous-vidéos."

#: ../../user_interface/menu/clip_menu/automaticscenesplit.rst:22
msgid ""
"Cut scenes are numbered and sorted under the clip in the project bin window "
"and will be saved with your project."
msgstr ""
"Les scènes coupées sont numérotées et triées sous la vidéo dans la fenêtre "
"du dossier du projet. Elles seront enregistrées avec votre projet."

#: ../../user_interface/menu/clip_menu/automaticscenesplit.rst:30
msgid ""
"This menu item is available from the Clip Jobs menu that appears when you :"
"ref:`project_tree` on a clip in the Project Bin."
msgstr ""
"Cet élément de menu est disponible à partir du menu « Tâches de vidéos », "
"s'affichant lorsque vous cliquez dans ref:`arborescence_projet` sur une "
"vidéo dans l'arborescence du projet."
