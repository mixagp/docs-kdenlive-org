# Xavier Besnard <xavier.besnard@neuf.fr>, 2021.
msgid ""
msgstr ""
"Project-Id-Version: Kdenlive Manual \n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2021-12-03 00:38+0000\n"
"PO-Revision-Date: 2021-12-06 15:12+0100\n"
"Last-Translator: Xavier Besnard <xavier.besnard@neuf.fr>\n"
"Language-Team: French <kde-francophone@kde.org>\n"
"Language: fr\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=(n > 1);\n"
"X-Generator: Lokalize 21.11.90\n"

#: ../../effects_and_compositions/transitions/alphaover.rst:10
msgid "alphaover transition"
msgstr "Transition « alphaover »"

#: ../../effects_and_compositions/transitions/alphaover.rst:12
msgid "Contents"
msgstr "Contenu"

#: ../../effects_and_compositions/transitions/alphaover.rst:14
msgid ""
"This is the `Frei0r alphaover <https://www.mltframework.org/plugins/"
"TransitionFrei0r-alphaover/>`_ MLT transition."
msgstr ""
"Ceci est la transition « Frei0r alphaover <https://www.mltframework.org/"
"plugins/TransitionFrei0r-alphaover/> »_ MLT."

#: ../../effects_and_compositions/transitions/alphaover.rst:16
msgid "The alpha OVER operation."
msgstr "L'opération « Alpha Over »."

#: ../../effects_and_compositions/transitions/alphaover.rst:18
msgid "Yellow clip has a triangle alpha shape with min = 0 and max =618."
msgstr "La vidéo jaune a une forme alpha en triangle avec min = 0 et max =618."

#: ../../effects_and_compositions/transitions/alphaover.rst:20
msgid "Green clip has rectangle alpha shape with min=0 and max =1000."
msgstr ""
"La vidéo verte a une forme alpha en rectangle avec min = 0 et max =1000."

#: ../../effects_and_compositions/transitions/alphaover.rst:22
msgid "alphaover is the transition in between."
msgstr "La transition « alphaover » est une transition intermédiaire."
