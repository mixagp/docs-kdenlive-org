# Xavier Besnard <xavier.besnard@neuf.fr>, 2021.
msgid ""
msgstr ""
"Project-Id-Version: Kdenlive Manual \n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2021-11-29 00:22+0000\n"
"PO-Revision-Date: 2021-12-06 17:59+0100\n"
"Last-Translator: Xavier Besnard <xavier.besnard@neuf.fr>\n"
"Language-Team: French <kde-francophone@kde.org>\n"
"Language: fr\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=(n > 1);\n"
"X-Generator: Lokalize 21.11.90\n"

#: ../../effects_and_compositions/effect_groups/colour_correction/white_balance_lms.rst:12
msgid "White Balance (LMS)"
msgstr "Balance des blancs (LMS)"

#: ../../effects_and_compositions/effect_groups/colour_correction/white_balance_lms.rst:14
msgid "Contents"
msgstr "Contenu"

#: ../../effects_and_compositions/effect_groups/colour_correction/white_balance_lms.rst:16
msgid ""
"This is the `Frei0r colgate <https://www.mltframework.org/plugins/"
"FilterFrei0r-colgate/>`_ MLT filter by Steiner H. Gunderson."
msgstr ""
"Ceci est le filtre « Frei0r colgate <https://www.mltframework.org/plugins/"
"FilterFrei0r-colgate/> »_ MLT développé par Steiner H. Gunderson."

#: ../../effects_and_compositions/effect_groups/colour_correction/white_balance_lms.rst:18
msgid "Do simple color correction, in a physically meaningful way."
msgstr ""
"Réalise une simple correction de couleurs, d'une façon physiquement "
"significative."

#: ../../effects_and_compositions/effect_groups/colour_correction/white_balance_lms.rst:20
msgid "**Parameters:**"
msgstr "**Paramètres :**"

#: ../../effects_and_compositions/effect_groups/colour_correction/white_balance_lms.rst:22
msgid ""
"Neutral Color: Choose a color from the source image that should be white."
msgstr ""
"Couleur neutre : sélectionnez une couleur à partir de l'image source, "
"représentant ce qui devrait être le blanc."

#: ../../effects_and_compositions/effect_groups/colour_correction/white_balance_lms.rst:24
msgid ""
"Color Temperature: Choose an output color temperature, if different from "
"6500 K."
msgstr ""
"Température de couleurs : sélectionnez une température de couleur de sortie, "
"si différente de 6500 K."
