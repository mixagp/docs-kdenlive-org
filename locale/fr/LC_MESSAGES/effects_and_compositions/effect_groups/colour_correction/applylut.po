# Xavier Besnard <xavier.besnard@neuf.fr>, 2021.
msgid ""
msgstr ""
"Project-Id-Version: Kdenlive Manual \n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2021-12-03 00:38+0000\n"
"PO-Revision-Date: 2021-12-14 08:39+0100\n"
"Last-Translator: Xavier Besnard <xavier.besnard@neuf.fr>\n"
"Language-Team: French <kde-francophone@kde.org>\n"
"Language: fr\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=(n > 1);\n"
"X-Generator: Lokalize 21.12.0\n"

#: ../../effects_and_compositions/effect_groups/colour_correction/applylut.rst:10
msgid "Apply LUT"
msgstr "Appliquer une table de conversion « LUT »"

#: ../../effects_and_compositions/effect_groups/colour_correction/applylut.rst:12
msgid "Contents"
msgstr "Contenu"

#: ../../effects_and_compositions/effect_groups/colour_correction/applylut.rst:14
msgid ""
"This is the `Avfilter lut3d <https://www.mltframework.org/plugins/"
"FilterAvfilter-lut3d/>`_ MLT filter."
msgstr ""
"Ceci est le filtre « Avfilter lut3d <https://www.mltframework.org/plugins/"
"FilterAvfilter-lut3d/> »_MLT."

#: ../../effects_and_compositions/effect_groups/colour_correction/applylut.rst:16
msgid ""
"Apply a 3D Look Up Table (LUT) to the video. A LUT is an easy way to correct "
"the color of a video."
msgstr ""
"Appliquez une table de conversion 3D (LUT) à la vidéo. Cette dernière est un "
"moyen facile de corriger la couleur d'une vidéo."

#: ../../effects_and_compositions/effect_groups/colour_correction/applylut.rst:18
msgid "**Supported formats:**"
msgstr "**Formats pris en charge : **"

#: ../../effects_and_compositions/effect_groups/colour_correction/applylut.rst:20
msgid ".3dl (AfterEffects), .cube (Iridas), .dat (DaVinci), .m3d (Pandora)"
msgstr ".3dl (AfterEffects), .cube (Iridas), .dat (DaVinci), .m3d (Pandora)"

#: ../../effects_and_compositions/effect_groups/colour_correction/applylut.rst:22
msgid "**Parameters:**"
msgstr "**Paramètres : **"

#: ../../effects_and_compositions/effect_groups/colour_correction/applylut.rst:24
msgid "Filename: File containing the LUT to be applied."
msgstr ""
"Nom de fichier : fichier contenant la table de conversion LUT » à appliquer."

#: ../../effects_and_compositions/effect_groups/colour_correction/applylut.rst:26
msgid ""
"Interpolation Method: Can be Nearest, Trilinear or Tetrahedral. Defaults to "
"Tetrahedral."
msgstr ""
"Méthode d'interpolation : peut être la plus proche, trilinéaire ou "
"tétraédrique. La valeur par défaut est tétraédrique."
