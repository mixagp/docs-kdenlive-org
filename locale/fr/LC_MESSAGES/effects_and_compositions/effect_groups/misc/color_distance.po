# Xavier Besnard <xavier.besnard@neuf.fr>, 2021.
msgid ""
msgstr ""
"Project-Id-Version: Kdenlive Manual \n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2021-11-18 00:21+0000\n"
"PO-Revision-Date: 2021-12-08 19:23+0100\n"
"Last-Translator: Xavier Besnard <xavier.besnard@neuf.fr>\n"
"Language-Team: French <kde-francophone@kde.org>\n"
"Language: fr\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=(n > 1);\n"
"X-Generator: Lokalize 21.11.90\n"

#: ../../effects_and_compositions/effect_groups/misc/color_distance.rst:13
msgid "Color Distance"
msgstr "Distance de couleurs"

#: ../../effects_and_compositions/effect_groups/misc/color_distance.rst:15
msgid "Contents"
msgstr "Contenu"

#: ../../effects_and_compositions/effect_groups/misc/color_distance.rst:17
msgid ""
"This is the `Frei0r colordistance <https://www.mltframework.org/plugins/"
"FilterFrei0r-colordistance/>`_ MLT filter."
msgstr ""
"Ceci est le filtre « Frei0r colordistance <https://www.mltframework.org/"
"plugins/FilterFrei0r-colordistance/> »_MLT."

#: ../../effects_and_compositions/effect_groups/misc/color_distance.rst:19
msgid ""
"Calculates the distance between the selected color and the current pixel and "
"uses that value as new pixel value."
msgstr ""
"Calcule la distance entre la couleur sélectionnée et le pixel courant et "
"utilise cette valeur comme la nouvelle valeur du pixel."

#: ../../effects_and_compositions/effect_groups/misc/color_distance.rst:21
msgid "https://youtu.be/eL8cFUJrUo0"
msgstr "https://youtu.be/eL8cFUJrUo0"

#: ../../effects_and_compositions/effect_groups/misc/color_distance.rst:23
msgid "https://youtu.be/4Ta9UE2nflU"
msgstr "https://youtu.be/4Ta9UE2nflU"

#: ../../effects_and_compositions/effect_groups/misc/color_distance.rst:25
msgid "https://youtu.be/7VRQyCUxYUQ"
msgstr "https://youtu.be/7VRQyCUxYUQ"
