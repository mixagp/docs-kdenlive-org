# Copyright (C) licensed under the  <a href="https://creativecommons.org/licenses/by-sa/4.0/">Creative Commons License SA 4.0</a> unless stated otherwise
# This file is distributed under the same license as the Kdenlive Manual package.
# Paolo Zamponi <zapaolo@email.it>, 2022.
#
msgid ""
msgstr ""
"Project-Id-Version: Kdenlive Manual \n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2022-01-16 00:37+0000\n"
"PO-Revision-Date: 2022-02-24 07:44+0100\n"
"Last-Translator: Paolo Zamponi <zapaolo@email.it>\n"
"Language-Team: Italian <kde-i18n-it@kde.org>\n"
"Language: it\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=(n != 1);\n"
"X-Generator: Lokalize 21.12.2\n"

#: ../../troubleshooting/installation_troubleshooting.rst:10
msgid "Installation Troubleshooting"
msgstr "Risoluzione dei problemi di installazione"

#: ../../troubleshooting/installation_troubleshooting.rst:12
msgid ""
"Many problems encountered in Kdenlive are caused by installation problems "
"(missing or mismatching packages). Kdenlive uses the MLT framework to "
"process all video operations, and MLT uses many other libraries like FFmpeg, "
"Frei0r, …"
msgstr ""
"Molti dei problemi con Kdenlive sono causati da problemi d'installazione "
"(pacchetti mancanti o non corrispondenti). Kdenlive utilizza il framework "
"MLT per per elaborare tutte le operazioni video, e MLT stesso utilizza molte "
"altre librerie, ad esempio FFmpeg, Frei0r, …"

#: ../../troubleshooting/installation_troubleshooting.rst:14
msgid ""
"Here are some tips to understand what might be wrong for you, depending on "
"the error message you get on startup. If this does not help you, check the "
"forums."
msgstr ""
"Ecco alcuni consigli per capire cosa può essere andato storto, in base al "
"messaggio di errore che hai avuto all'avvio. Se questi non aiutano, "
"controlla nei forum."

#: ../../troubleshooting/installation_troubleshooting.rst:17
msgid "Your MLT installation cannot be found or Cannot start MLT backend"
msgstr ""
"L'installazione di MLT non è stata trovata, oppure Impossibile avviare il "
"motore MLT"

#: ../../troubleshooting/installation_troubleshooting.rst:19
msgid ""
"There is obviously something wrong with your MLT installation. Either it is "
"not installed or not in a standard location. You can test your MLT "
"installation from a terminal, type: `melt color:red` This should bring up a "
"red window (press :kbd:`q` to close it)."
msgstr ""
"Ovviamente qualcosa è andato storto nell'installazione di MLT: o non è "
"installato, oppure non lo è in una posizione standard. Puoi verificare "
"l'installazione di MLT da terminale: `melt color:red` dovrebbe creare una "
"finestra rossa (premi :kbd:`q` per chiuderla)."

#: ../../troubleshooting/installation_troubleshooting.rst:22
msgid ""
"If you see an error message, try reinstalling MLT or check that you don't "
"have several versions installed on the system."
msgstr ""
"Se vedi un messaggio di errore, prova a reinstallare MLT, oppure controlla "
"di non aver installato più versioni nel sistema."

#: ../../troubleshooting/installation_troubleshooting.rst:24
msgid ""
"If you see the red window, check where your MLT is installed: `which melt`. "
"Then delete Kdenlive's config file (`$HOME/.config/kdenliverc`) and restart "
"Kdenlive."
msgstr ""
"Se invece vedi la finestra rossa, controlla dove è installato MLT: `which "
"melt`. Elimina quindi il file di configurazione di Kdenlive (`$HOME/.config/"
"kdenliverc`), quindi riavvia Kdenlive."

#: ../../troubleshooting/installation_troubleshooting.rst:27
msgid "Missing package"
msgstr "Pacchetto mancante"

#: ../../troubleshooting/installation_troubleshooting.rst:29
msgid "A dependency is missing and it is recommended to install it."
msgstr "Una dipendenza è mancante, ed è raccomandato installarla."

#: ../../troubleshooting/installation_troubleshooting.rst:32
msgid "Frei0r"
msgstr "Frei0r"

#: ../../troubleshooting/installation_troubleshooting.rst:34
msgid ""
"This package provides many effects and transitions. Without it, Kdenlive's "
"features will be reduced. You can simply install frei0r-plugins from your "
"package manager."
msgstr ""
"Questo pacchetto fornisce molti effetti e transizioni: senza di esso, le "
"funzionalità di Kdenlive saranno ridotte. Puoi semplicemente installare "
"frei0r-plugins dal gestore di pacchetti."

#: ../../troubleshooting/installation_troubleshooting.rst:37
msgid "Breeze icons"
msgstr "Icone Brezza"

#: ../../troubleshooting/installation_troubleshooting.rst:39
msgid ""
"Many icons used by Kdenlive come from the Breeze Icons package. Without it, "
"many parts of the UI will not appear correctly. You can simply install "
"breeze-icon-theme or breeze-icons from your package manager."
msgstr ""
"Molte delle icone utilizzate da Kdenlive provengono dal pacchetto di icone "
"Brezza: senza di esso, diverse parti dell'interfaccia non appariranno "
"correttamente. Puoi semplicemente installare breeze-icon-theme, oppure "
"breeze-icons, dal gestore di pacchetti."

#: ../../troubleshooting/installation_troubleshooting.rst:42
msgid "MediaInfo"
msgstr "MediaInfo"

#: ../../troubleshooting/installation_troubleshooting.rst:44
msgid ""
"Download and install MediaInfo from `here <https://mediaarea.net/MediaInfo/"
"Download>`_"
msgstr ""
"Scarica e installa MediaInfo da `qui <https://mediaarea.net/MediaInfo/"
"Download>`_"

#: ../../troubleshooting/installation_troubleshooting.rst:47
msgid "Missing MLT module"
msgstr "Modulo di MLT mancante"

#: ../../troubleshooting/installation_troubleshooting.rst:49
msgid "An MLT dependency is missing and it is required to install it."
msgstr "Una dipendenza di MLT è mancante, ed è richiesta l'installazione."

#: ../../troubleshooting/installation_troubleshooting.rst:51
msgid ""
"**SDL** is used to output audio. Install `libsdl 1.x`` from your package "
"manager."
msgstr ""
"**SDL** è utilizzato per produrre l'audio. Installa `libsdl 1.x` dal gestore "
"di pacchetti."

#: ../../troubleshooting/installation_troubleshooting.rst:53
msgid ""
"**Avformat** is the FFmpeg module. Make sure you have ffmpeg installed on "
"your system."
msgstr ""
"**Avformat** è il modulo FFmpeg. Assicurati che ffmpeg sia installato nel "
"sistema."

#: ../../troubleshooting/installation_troubleshooting.rst:56
msgid "The following codecs were not found on your system…"
msgstr "I seguenti codec non sono stati trovati nel sistema..."

#: ../../troubleshooting/installation_troubleshooting.rst:58
msgid ""
"Some audio / video codecs are not installed by default. Installing a package "
"called `libavcodec-extra` might solve the problem."
msgstr ""
"Per impostazione predefinita, alcuni codec audio e video non vengono "
"installati. L'installazione di un pacchetto chiamato `libavcodec-extra` "
"dovrebbe risolvere il problema."

#: ../../troubleshooting/installation_troubleshooting.rst:60
msgid ""
"On openSuse, you need to add the `packman repository <https://www.opensuse-"
"community.org/>`_, then enable `replace vendor package <https://en.opensuse."
"org/SDB:Vendor_change_update#Full_repository_Vendor_change>`_ on the packman "
"repository."
msgstr ""
"In openSuse devi aggiungere `packman repository <https://www.opensuse-"
"community.org/>`_, poi abilitare l'opzione `passa i pacchetti di sistema "
"<https://it.opensuse.org/SDB:"
"Aggiornamento_con_cambio_di_fornitore#Cambio_di_fornitore_per_tutto_il_repository>`_ "
"in packman repository."
