# Copyright (C) licensed under the  <a href="https://creativecommons.org/licenses/by-sa/4.0/">Creative Commons License SA 4.0</a> unless stated otherwise
# This file is distributed under the same license as the Kdenlive Manual package.
# Paolo Zamponi <zapaolo@email.it>, 2021.
#
msgid ""
msgstr ""
"Project-Id-Version: Kdenlive Manual \n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2021-11-18 00:21+0000\n"
"PO-Revision-Date: 2021-12-12 19:37+0100\n"
"Last-Translator: Paolo Zamponi <zapaolo@email.it>\n"
"Language-Team: Italian <kde-i18n-it@kde.org>\n"
"Language: it\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=(n != 1);\n"
"X-Generator: Lokalize 21.08.3\n"

#: ../../effects_and_compositions/effect_groups/colour/techicolor.rst:13
msgid "Technicolor"
msgstr "Technicolor"

#: ../../effects_and_compositions/effect_groups/colour/techicolor.rst:15
msgid "Contents"
msgstr "Contenuto"

#: ../../effects_and_compositions/effect_groups/colour/techicolor.rst:17
msgid ""
"This is the `Tcolor <https://www.mltframework.org/plugins/FilterTcolor/>`_ "
"MLT filter."
msgstr ""
"Usa il filtro MLT `Tcolor <https://www.mltframework.org/plugins/FilterTcolor/"
">`_."

#: ../../effects_and_compositions/effect_groups/colour/techicolor.rst:19
msgid "https://youtu.be/hDLp5IymciA"
msgstr "https://youtu.be/hDLp5IymciA"
