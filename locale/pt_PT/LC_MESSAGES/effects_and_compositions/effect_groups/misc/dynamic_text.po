# SOME DESCRIPTIVE TITLE.
# Copyright (C) licensed under the  <a href="https://creativecommons.org/licenses/by-sa/4.0/">Creative Commons License SA 4.0</a> unless stated otherwise
# This file is distributed under the same license as the Kdenlive Manual package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
msgid ""
msgstr ""
"Project-Id-Version: Kdenlive Manual \n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2021-12-03 00:38+0000\n"
"PO-Revision-Date: 2021-12-07 18:29+0000\n"
"Last-Translator: José Nuno Coelho Pires <zepires@gmail.com>\n"
"Language-Team: Portuguese <kde-i18n-pt@kde.org>\n"
"Language: pt\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-POFile-SpellExtra: clip menuselection clips ref\n"

#: ../../effects_and_compositions/effect_groups/misc/dynamic_text.rst:11
msgid "Dynamic Text"
msgstr "Texto Dinâmico"

#: ../../effects_and_compositions/effect_groups/misc/dynamic_text.rst:13
msgid "Contents"
msgstr "Conteúdo"

#: ../../effects_and_compositions/effect_groups/misc/dynamic_text.rst:15
msgid ""
"The \"Dynamic Text\" effect allows you to overlay a timecode - which counts "
"up relative to the start of the clip or timeline it is applied to."
msgstr ""
"O efeito de \"Texto Dinâmico\" permite-lhe sobrepor um código temporal - que "
"é relativo ao início do 'clip' ou da linha temporal em que se aplica."

#: ../../effects_and_compositions/effect_groups/misc/dynamic_text.rst:17
msgid ""
"Hint: You can add effects to entire video tracks by clicking on the **Track "
"Header** and choosing :menuselection:`Timeline --> Add Effect`. Video tracks "
"that have effects added to them are marked with a yellow star in the Track "
"Header."
msgstr ""
"Sugestão: Poderá adicionar efeitos às faixas de vídeo inteiras, carregando "
"no **Cabeçalho da Faixa** e escolhendo a opção :menuselection:`Linha "
"Temporal --> Adicionar um Efeito`. As faixas de vídeo que têm efeitos "
"adicionados a elas aparecem com uma estrela amarela no Cabeçalho da Faixa."

#: ../../effects_and_compositions/effect_groups/misc/dynamic_text.rst:23
msgid ""
"If you add this effect to the Video track rather than individual clips the "
"timecode will not reset at the beginning of the next clip but rather count "
"across the whole length of your project."
msgstr ""
"Se adicionar este efeito à faixa de vídeo em vez de aos 'clips' individuais, "
"o código temporal não será reposto no início do 'clip' seguinte, mas irá "
"assim contar com a duração total do seu projecto."

#: ../../effects_and_compositions/effect_groups/misc/dynamic_text.rst:25
msgid ""
"See also the :ref:`render` option in the render dialog to add time code or "
"frame count to the entire rendered project."
msgstr ""
"Veja também a opção :ref:`render` na janela de desenho para adicionar o "
"código temporal ou um dado número de imagens do projecto inteiro."

#: ../../effects_and_compositions/effect_groups/misc/dynamic_text.rst:27
msgid "https://youtu.be/A4ObXRhi6ZM"
msgstr "https://youtu.be/A4ObXRhi6ZM"
