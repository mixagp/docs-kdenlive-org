# SOME DESCRIPTIVE TITLE.
# Copyright (C) licensed under the  <a href="https://creativecommons.org/licenses/by-sa/4.0/">Creative Commons License SA 4.0</a> unless stated otherwise
# This file is distributed under the same license as the Kdenlive Manual package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
msgid ""
msgstr ""
"Project-Id-Version: Kdenlive Manual \n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2021-11-18 00:21+0000\n"
"PO-Revision-Date: 2021-11-20 17:59+0000\n"
"Last-Translator: José Nuno Coelho Pires <zepires@gmail.com>\n"
"Language-Team: Portuguese <kde-i18n-pt@kde.org>\n"
"Language: pt\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"

#: ../../effects_and_compositions/effect_groups/artistic/binarize.rst:10
msgid "Binarize"
msgstr "Binário"

#: ../../effects_and_compositions/effect_groups/artistic/binarize.rst:12
msgid "Contents"
msgstr "Conteúdo"

#: ../../effects_and_compositions/effect_groups/artistic/binarize.rst:14
msgid "Create a black and white image."
msgstr "Criar uma imagem a preto-e-branco."
