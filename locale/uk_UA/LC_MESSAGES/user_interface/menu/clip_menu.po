# Copyright (C) licensed under the  <a href="https://creativecommons.org/licenses/by-sa/4.0/">Creative Commons License SA 4.0</a> unless stated otherwise
# This file is distributed under the same license as the Kdenlive Manual package.
#
# Yuri Chornoivan <yurchor@ukr.net>, 2021.
msgid ""
msgstr ""
"Project-Id-Version: Kdenlive Manual \n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2021-12-21 00:37+0000\n"
"PO-Revision-Date: 2021-12-21 08:41+0200\n"
"Last-Translator: Yuri Chornoivan <yurchor@ukr.net>\n"
"Language-Team: Ukrainian <kde-i18n-uk@kde.org>\n"
"Language: uk\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=4; plural=n==1 ? 3 : n%10==1 && n%100!=11 ? 0 : n"
"%10>=2 && n%10<=4 && (n%100<10 || n%100>=20) ? 1 : 2;\n"
"X-Generator: Lokalize 20.12.0\n"

#: ../../user_interface/menu/clip_menu.rst:16
msgid "Clip Menu"
msgstr "Меню «Кліп»"

#: ../../user_interface/menu/clip_menu.rst:18
msgid "Contents"
msgstr "Зміст"

#: ../../user_interface/menu/clip_menu.rst:20
msgid ""
"The functions controlled from this menu affect the clip that is selected in "
"the timeline. This is in contrast to :ref:`project_menu` functions which "
"affect the clips selected in the Project Bin."
msgstr ""
"Пункти цього меню керують кліпом, який позначено на монтажному столі, на "
"відміну від пунктів :ref:`project_menu`, які керують кліпами, які позначено "
"на панелі контейнера проєкту."

#: ../../user_interface/menu/clip_menu.rst:29
msgid "Markers Menu Item"
msgstr "Пункт меню «Позначки»"

#: ../../user_interface/menu/clip_menu.rst:31
msgid ""
"The menu allows you to :menuselection:`Add`, :menuselection:`Edit` and :"
"menuselection:`Delete` in :ref:`markers`."
msgstr ""
"За допомогою цього меню ви можете :menuselection:`Додати`, :menuselection:"
"`Змінити` і :menuselection:`Вилучити` :ref:`позначки <markers>`"

#: ../../user_interface/menu/clip_menu.rst:34
msgid "Automatic Transition"
msgstr "Автоматичний перехід"

#: ../../user_interface/menu/clip_menu.rst:36
msgid ""
"When a transition is selected, this menu item allows you toggle the "
"transition to and from :ref:`transitions` mode."
msgstr ""
"Якщо позначено перехід, за допомогою цього пункту меню можна увімкнути або "
"вимкнути режим автоматичного :ref:`переходу <transitions>`."

#: ../../user_interface/menu/clip_menu.rst:39
msgid "Other Items"
msgstr "Інші пункти"

#: ../../user_interface/menu/clip_menu.rst:41
msgid ""
"The other menu items which appear when in the Clip menu are also available "
"from the :ref:`project_tree`."
msgstr ""
"Інші пункти меню «Кліп» є пунктами, доступ до яких можна отримати за "
"допомогою контекстного меню :ref:`контейнера проєкту <project_tree>`."

#: ../../user_interface/menu/clip_menu.rst:43
msgid ":ref:`extract_audio`"
msgstr ":ref:`extract_audio`"

#: ../../user_interface/menu/clip_menu.rst:45
msgid ":menuselection:`Clip Jobs`"
msgstr ":menuselection:`Обробка кліпів`"

#: ../../user_interface/menu/clip_menu.rst:47
msgid ":ref:`stabilize`"
msgstr ":ref:`stabilize`"

#: ../../user_interface/menu/clip_menu.rst:49
msgid ":ref:`automaticscenesplit`"
msgstr ":ref:`automaticscenesplit`"

#: ../../user_interface/menu/clip_menu.rst:51
msgid ":ref:`duplicate_clip_with_speed_change`"
msgstr ":ref:`duplicate_clip_with_speed_change`"

#: ../../user_interface/menu/clip_menu.rst:53
msgid ":ref:`transcode`"
msgstr ":ref:`transcode`"

#: ../../user_interface/menu/clip_menu.rst:55
msgid ":ref:`Locate Clip... <locate_clip>`"
msgstr ":ref:`Знайти кліп… <locate_clip>`"

#: ../../user_interface/menu/clip_menu.rst:57
msgid ":ref:`Reload Clip <reload_clip>`"
msgstr ":ref:`Перезавантажити кліп <reload_clip>`"

#: ../../user_interface/menu/clip_menu.rst:59
msgid ":ref:`Duplicate Clip <duplicate_clip>`"
msgstr ":ref:`Дублювати кліп <duplicate_clip>`"

#: ../../user_interface/menu/clip_menu.rst:61
msgid ":ref:`Proxy Clip <proxy_clip>`"
msgstr ":ref:`Проміжний кліп <proxy_clip>`"

#: ../../user_interface/menu/clip_menu.rst:63
msgid ":ref:`clip_in_timeline`"
msgstr ":ref:`clip_in_timeline`"

#: ../../user_interface/menu/clip_menu.rst:65
msgid ":ref:`Clip Properties <clip_properties>`"
msgstr ":ref:`Властивості кліпу <clip_properties>`"

#: ../../user_interface/menu/clip_menu.rst:67
msgid ":ref:`edit_clip`"
msgstr ":ref:`edit_clip`"

#: ../../user_interface/menu/clip_menu.rst:69
msgid ":ref:`Rename... <rename_clip>`"
msgstr ":ref:`Перейменувати… <rename_clip>`"

#: ../../user_interface/menu/clip_menu.rst:71
msgid ":ref:`delete_clip`"
msgstr ":ref:`delete_clip`"

#: ../../user_interface/menu/clip_menu.rst:73
msgid "Contents:"
msgstr "Зміст:"

#~ msgid ":ref:`clips`"
#~ msgstr ":ref:`clips`"
