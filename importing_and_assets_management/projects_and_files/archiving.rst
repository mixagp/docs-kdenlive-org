.. metadata-placeholder

   :authors: - Annew (https://userbase.kde.org/User:Annew)
             - Neverendingo (https://userbase.kde.org/User:Neverendingo)
             - Claus Christensen
             - Yuri Chornoivan
             - Jean-Baptiste Mardelle <jb@kdenlive.org>
             - Ttguy (https://userbase.kde.org/User:Ttguy)
             - Tenzen (https://userbase.kde.org/User:Tenzen)

   :license: Creative Commons License SA 4.0

.. _archiving:

Archiving
=========

.. contents::

.. image:: /images/Archive.png
   :align: left
   :alt: Archive

The Archiving feature (:menuselection:`Project --> Archive Project`, see :ref:`project_menu`) in **Kdenlive** allows you to copy all files required by the project (images, video clips, project files,...) to a folder, and alternatively to compress the whole into a tar.gz or a zip file.

Archiving changes the project file to update the path of video clips to the archived versions.

This can be useful if you finished working on a project and want to keep a copy of it, or if you want to move a project from one computer to another.

The resulting tar.gz file can be opened directly in **Kdenlive**.  Kdenlive will uncompress it to a location you specify before opening it.


