.. metadata-placeholder

   :authors: - Annew (https://userbase.kde.org/User:Annew)
             - Claus Christensen
             - Yuri Chornoivan
             - Ttguy (https://userbase.kde.org/User:Ttguy)
             - Bushuev (https://userbase.kde.org/User:Bushuev)
             - Jack (https://userbase.kde.org/User:Jack)

   :license: Creative Commons License SA 4.0

.. _settings_menu:

Settings Menu
=============

.. contents::


.. image:: /images/Settings_menu.png
  :align: left
  :alt: Settings menu


* :ref:`manage_project_profiles`
* :ref:`download_new_wipes`
* :ref:`download_new_render_profiles`
* :ref:`download_new_project_profiles`
* :ref:`download_new_title_templates`
* :ref:`run_config_wizard`
* :ref:`themes`
* :ref:`toolbars_shown`
* :ref:`full_screen_mode`
* :ref:`configure_shortcuts`
* :ref:`toolbars`
* :ref:`configure_notifications`
* :ref:`configure_kdenlive`

.. rst-class:: clear-both

Settings Menu - Mac OS X
------------------------

.. image:: /images/Kdenlive_Settings_OSX.png
   :align: left
   :alt: Settings OSX

On the Mac OS X build of Kdenlive the :menuselection:`Settings` menu does not contain the :menuselection:`Configure Kdenlive` menu item. The equivalent on Mac OS X is the :menuselection:`Preferences` menu item found under the :menuselection:`Kdenlive` menu.

.. rst-class:: clear-both

.. image:: /images/Kdenlive_Preferences_OSX.png
   :alt: Preference OSX

.. toctree::
   :caption: Contents:
   :glob:

   settings_menu/*
